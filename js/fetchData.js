async function getIP() {
    try {
        const data = await fetch('https://api.ipify.org/?format=json');
        const response = await data.json();
        return response.ip;

    } catch (err) {
        console.log(err);
    }
}

export async function userInfo() {
    const ip = await getIP();
    try {
        const infoData = await fetch(`http://ip-api.com/json/${ip}`);
        const info = await infoData.json();
        return { ip, info };
    } catch (err) {
        console.log(err);
    }
}

