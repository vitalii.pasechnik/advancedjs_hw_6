export const createLoader = () => {
    const loader = document.createElement('div');
    loader.classList.add('loader');
    Array(5).fill(1).forEach((_, i) => {
        loader.insertAdjacentHTML('beforeend', `<div class="item item_${i + 1}"></div>`)
    })
    return loader;
}