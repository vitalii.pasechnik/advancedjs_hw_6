import { createLoader } from "./createLoader.js";

export const createList = (ip, info, container = document.body) => {
    const list = document.createElement('ul');
    list.classList.add('list');
    list.insertAdjacentHTML('beforeend', `<button class='btn'>Я знайду тебе по IP</button>`);
    container.append(list);

    const btn = container.querySelector('.btn');
    btn.addEventListener('click', () => {
        btn.remove();
        list.append(createLoader());
        const loader = list.querySelector('.loader');
        setTimeout(() => {
            loader.remove();
            loadUserInfo(ip, info, list);
        }, 2000)
    });
}

function loadUserInfo(ip, info, parent = document.body) {
    parent.insertAdjacentHTML('beforeend', `
        <li>Ваш IP: <b>${ip}</b></li>
        <li>Ваш провайдер: <b>${info.isp}</b></li>
        <li>Часовий пояс: <b>${info.timezone}</b></li>
        <li>Країна: <b>${info.country}</b></li>
        <li>Регіон: <b>${info.regionName}</b></li>
        <li>Місто: <b>${info.city}</b></li>
    `)
}