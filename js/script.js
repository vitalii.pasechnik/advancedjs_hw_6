
import { userInfo } from "./fetchData.js";
import { createList } from "./createList.js";


const container = document.querySelector('#root');

async function renderInfo() {
    const { ip, info } = await userInfo();
    createList(ip, info, container);
}

renderInfo();